dr-playbook API
===================

  URL: https://dr-playbook.herokuapp.com

##Test App

A user must be authenticated before performing any actions. To authenticate a user:

  POST /v1/authenticate?email=#{email}&password=#{password}

This response returns a JWT in the AuthToken parameter:
  {
       "auth_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE0Nzg1NTA0MTR9.UMHJUGE0Xx1Lr1xieOvwIe9mybBBGARbt0u3Ny2mT6A"
  }

This parameter must be included in an Authorizaton header in each request. Tokens last 24h.

###Testing Credentials:
  POST /v1/authenticate?email=em@il.com&password=password

----------


User
-------------

### Create

**Path:**

  POST /v1/users/

**Example:**

  post '/v1/users/', 
      params: { 
        user: { 
          name: @name, 
          email: @email, 
          password: @password, 
        },
        signup_code: @signup_code 
      }, headers: @auth_token

Users need a signup code to successfully create an account. 

### Read

**Path:**

  GET /v1/users/(?role=role)
  GET /v1/users/:id

**Example:**

  get '/v1/users/#{:id}', 
      params: { }, 
      headers: @auth_token


Alerts
---------------

Directions (based off of 'A better way' snippet in `angular-actioncable`):

# Grab a JWT to use by via `POST https://dr-playbook.herokuapp.com/v1/authenticate?email=em@il.com&password=password` (#{JWT}). Also make note of the user_id here (#{user_id})

# Put "wss://dr-playbook.herokuapp.com/v1/cable?jwt=#{JWT}" in the `meta` tag under the `content` attribute.

# Subscribe to the channel "AlertsChannel" by the following code(?):

`new ActionCableChannel("AlertsChannel", {user_id: #{user_id} });`

# Try `POST`ing a message to `POST https://dr-playbook.herokuapp.com/v1/users/#{user_id}/alerts` with the following form_data:

````
{
  "alert": {
    "message_title":"Test",
    "message_content":"Test2"
  }
}
````

# If all goes well, you'll get an Alert object back as a response to the API and a message in the channel you've subscribed to. 

````
# == Schema Information
#
# Table name: alerts
#
#  id              :integer          not null, primary key
#  resource_type   :string(255)
#  resource_id     :integer
#  recipient_type  :string(255)      not null
#  recipient_id    :integer          not null
#  message_title   :text(65535)
#  message_content :text(65535)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
````

- Alerts are created by `POST`ing to a route that follows this pattern:
````
POST /v1/:recipient_type/:recipient_id/alerts
````
where `:recipient_type` can be either an `Office` or `User`.

## Attributes

- `message_title`
- `message_content`
- `resource_type `(only "Patient" for now, not sure if you guys needed to pass along the patient object to the alert to link to them or something
- `resource_id` (the patient ID)

## Example
````
POST /v1/users/1/alerts HTTP/1.1
Host: dr-playbook.herokuapp.com
Authorization: (jwt token)
Content-Type: application/json
Cache-Control: no-cache
Postman-Token: 3fa670a1-b9c5-1969-d46a-755d3b454a4c

{
  "alert": {
    "message_title": "Test Title",
    "message_content": "Test Content"
  }
}
````

When an `Alert` is created, a job is queued to broadcast the alert to all appropriate listening `User`s. 

## Destroying Alerts
````
DELETE /alerts/:id
````
Note, that you don't need to specify the recipient in the route, just the `id` of the `Alert` itself.
