require 'houston'

class BroadcastPatientJob < ActiveJob::Base
  queue_as :default

  def perform(patient, action)
    p "IN JOB"
    if patient.class.name == "Patient"
      p "Patient is patient"
      ActionCable.server.broadcast "patients_#{patient.office.id}",
      { patient: PatientSerializer.new(patient), action: action }
    else
      p "Patient is hash"
      ActionCable.server.broadcast "patients_#{patient['office_id']}",
      { patient: patient, action: action }
    end
  end
end
