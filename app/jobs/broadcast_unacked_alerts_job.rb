class BroadcastUnackedAlertsJob < ActiveJob::Base
  queue_as :default
  
  def perform(user_id)
      
    @user = User.find(user_id)

    @user.unacked_alerts.each do |ua|
      ActionCable.server.broadcast "alerts_#{@user.id}",
          alert: AlertSerializer.new(ua)
      # @user.try_apn(ua)
    end
  end
end
