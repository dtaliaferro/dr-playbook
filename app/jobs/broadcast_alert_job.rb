require 'houston'

class BroadcastAlertJob < ActiveJob::Base
  queue_as :default


    
  def perform(alert)
    if alert.recipient_type == "User"
      ActionCable.server.broadcast "alerts_#{alert.recipient_id}",
      { alert: AlertSerializer.new(alert) }
      p "trying apn out"
      User.find(alert.recipient_id).try_apn(alert)
    elsif alert.recipient_type == "Office"
      User.where(office_id: alert.recipient_id).where(role: :doctor).each do |d|
        ActionCable.server.broadcast "alerts_#{d.id}",
          alert: AlertSerializer.new(alert)
        d.try_apn(alert)
      end
    end
  end
end
