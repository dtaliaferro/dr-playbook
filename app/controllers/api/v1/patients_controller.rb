module Api::V1
  class PatientsController < ApplicationController
    before_action :set_patient, only: [:show, :update, :destroy]

    # GET /patients
    def index
      @patients = Patient.all

      render json: @patients
    end

    # GET /patients/1
    def show
      render json: @patient
    end

    # POST /patients
    def create
      flag_columns = Patient.flag_columns.map(&:to_sym)
      @patient = Patient.new(patient_params.except(*flag_columns))
      @patient_flag_params = patient_params.slice(*flag_columns)

      # it's safer to iterate this way so unpermitted values aren't saved 
      # to the DB

      Patient.flag_mapping.each do |f| 
        # f = ["treatments", {:fmx=>1, :pano=>2, :bwx=>4, :pas=>8, :pro=>16, :perio=>32}]
        attribute_name = f[0]
        #p @patient_flag_params.inspect + ' include? ' + f[0].to_s
        if @patient_flag_params.include?(f[0].to_s)
          f[1].each do |fa| # fa = { fmx: 1 }
            flag_name = fa[0]
            #p @patient_flag_params[attribute_name].to_s + ' include? ' + fa[0].to_s
            if @patient_flag_params[attribute_name].include?(fa[0].to_s)
              #p fa[0].to_s + ' is true'
              @patient.send(fa[0].to_s + '=', 1)
            else
              #p fa[0].to_s + ' is false'
              @patient.send(fa[0].to_s + '=', 0)
            end
          end
        end
      end


      if @patient.save
        render json: @patient, status: :created
      else
        render json: @patient.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /patients/1
    def update
      flag_columns = Patient.flag_columns.map(&:to_sym)
      @patient_flag_params = patient_params.slice(*flag_columns)
      #p 'FLAG PARAMS:' + @patient_flag_params.inspect
      Patient.flag_mapping.each do |f| 
        # f = ["treatments", {:fmx=>1, :pano=>2, :bwx=>4, :pas=>8, :pro=>16, :perio=>32}]
        attribute_name = f[0]
        #p @patient_flag_params.inspect + ' include? ' + f[0].to_s
        if @patient_flag_params.include?(f[0].to_s)
          f[1].each do |fa| # fa = { fmx: 1 }
            flag_name = fa[0]
            #p @patient_flag_params[attribute_name].to_s + ' include? ' + fa[0].to_s
            if @patient_flag_params[attribute_name].include?(fa[0].to_s)
              #p fa[0].to_s + ' is true'
              @patient.send(fa[0].to_s + '=', 1)
            else
              #p fa[0].to_s + ' is false'
              @patient.send(fa[0].to_s + '=', 0)
            end
          end
        end
      end

      if @patient.update(patient_params.except(*flag_columns))
        render json: @patient
      else
        render json: @patient.errors, status: :unprocessable_entity
      end
    end

    # DELETE /patients/1
    def destroy
      @patient.destroy
    end

    def delete_all
      o = Office.find(params[:office_id])

      Patient.where(office_id: o.id).destroy_all
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_patient
        @patient = Patient.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def patient_params
        params.require(:patient).permit(:name, :status, :pt_parent, :staff_id, 
          :exam_type, :pt_notes, :referral, :arrived_at, :appointment_at, 
          :departure_at, :room, :age, :blood_pressure, :doctor_id, :procedure, 
          :health_history, :dr_history, :complaint, :concerns, :concerns_other, 
          :ocs, :tmj, :perio, :imaging, :office_id, :next_visit,
          :same_day_treatment, treatments: [], objections: [], pt_misc: [], 
          values: [], past_doctor_ids: [], )
      end
  end
end