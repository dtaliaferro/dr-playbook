module Api::V1
  class AlertsController < ApplicationController
    before_action :set_alert, only: [:show, :update, :destroy]
    # before_action :get_resource

    # GET /alerts
    def index
      if params[:user_id]
        @alerts = Alert.filter(recipient_type: 'User', recipient_id: params[:user_id])
      elsif params[:office_id]
        @alerts = Alert.filter(recipient_type: 'Office', recipient_id: params[:office_id])
      else
        Alert.all
      end
      
      render json: @alerts
    end

    # GET /alerts/1
    def show
      render json: @alert
    end


    # POST /alerts
    def create
      @alert = get_recipient.alerts.new(alert_params)
      
      if @alert.save
        render json: @alert, status: :created
      else
        render json: @alert.errors, status: :unprocessable_entity
      end
    end

=begin
    # PATCH/PUT /alerts/1
    def update
      if @alert.update(alert_params)
        render json: @alert
      else
        render json: @alert.errors, status: :unprocessable_entity
      end
    end
=end
    # DELETE /alerts/1
    def destroy
      @alert.destroy
    end

    def dismiss
      @alert_ack = AlertAck.new(recipient_type: 'User', 
        recipient_id: current_user.id, alert_id: params[:alert_id])

      if @alert_ack.save
        render json: @alert_ack, status: :accepted
      else
        render json: @alert_ack.errors, status: :unprocessable_entity
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_alert
        @alert = Alert.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def alert_params
        params.require(:alert).permit(:message_title, :message_content, 
          :recipient_type, :recipient_id, :resource_type, :resource_id)
      end

      def get_resource
        if params[:patient_id]
          @resource = Patient.find(params[:patient_id])
        end
      end

      def get_resource_path_name
        @resource.class.name.downcase
      end

      def get_recipient
        recipient_class = params[:model_name].constantize
        recipient_foreign_key = params[:model_name].foreign_key
        @recipient = recipient_class.find(params[recipient_foreign_key.to_sym])
      end

      def get_recipient_path_name
        @recipient.class.name.downcase
      end

  end
end