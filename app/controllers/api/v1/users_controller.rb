module Api::V1
  class UsersController < ApiController
    before_action :set_user, only: [:show, :update, :destroy]


    # GET /users
    def index

      @filter_params = params.slice(:office_id, :role)

      # have to convert enums to their proper number
      @filter_params[:role] = User.roles[@filter_params[:role]]
      
      @users = User.filter(@filter_params)

      render json: @users
    end

    # GET /users/1
    def show
      render json: @user
    end

    # POST /users
    def create
      @user = User.new(user_params)

      # verify_signup_code

      if @user.save
        render json: @user, status: :created, location: v1_user_path(@user)
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /users/1
    def update
      if @user.update(user_params)
        render json: @user
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end

    # DELETE /users/1
    def destroy
      @user.destroy

      render json: {}, status: :ok
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:name, :email, :password, :role, :office_id,
        :token)
    end

    # since we're not authenticating user creation with a JWT header,
    # we have to authenticate with the provided signup code instead.
    def verify_signup_code
      signup_code = params[:signup_code]

      @i = Invitation.where(signup_code: signup_code).try(:first)

      if @i
        @user.role = @i.role
        @user.office = @i.office
      else
        @user.errors.add(:signup_code, 'Invalid signup code')
      end
    end

  end
end