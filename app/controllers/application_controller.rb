class ApplicationController < ActionController::API
  include ActionController::Serialization
  
  before_action :authenticate_request

  attr_reader :current_user

  private

  def authenticate_request
    @current_user = AuthorizeApiRequest.call(request.headers).result
    if @current_user.nil?
      render json: { error: 'Not Authorized' }, status: 401
    elsif ! @current_user.office.active?
      render json: { error: 'Office Deactivated' }, status: 401
    end
  end
end
