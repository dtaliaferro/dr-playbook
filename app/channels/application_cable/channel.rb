module ApplicationCable
  class Channel < ActionCable::Channel::Base

    def user 
      @user ||= User.find(decoded_auth_token[:user_id]) if decoded_auth_token 
      p @user.inspect
      @user
    end 

    def decoded_auth_token 
      @decoded_auth_token ||= JsonWebToken.decode(jwt_param) 
    end 

    # grab the token from the auth header
    def jwt_param 
      if jwt.present? 
        return jwt.split(' ').last 
      end 
      nil 
    end 

    def jwt
      params.fetch('data').fetch('jwt')
    end
  end
end
