class AlertsChannel < ApplicationCable::Channel
  def subscribed
    stream_from stream_name
    BroadcastUnackedAlertsJob.perform_later(user.id)
  end

  protected

  def stream_name
    "alerts_#{user.id}"
  end

  #def user_id
  #  params.fetch('data').fetch('user_id')
  #end

  def user 
    @user ||= User.find(decoded_auth_token[:user_id]) if decoded_auth_token 
    p @user.inspect
    @user
  end 

  def decoded_auth_token 
    @decoded_auth_token ||= JsonWebToken.decode(jwt_param) 
  end 

  # grab the token from the auth header
  def jwt_param 
    if jwt.present? 
      return jwt.split(' ').last 
    end 
    nil 
  end 

  def jwt
    params.fetch('data').fetch('jwt')
  end

end