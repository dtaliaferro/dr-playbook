class PatientsChannel < ApplicationCable::Channel
  def subscribed
    stream_from stream_name
  end

  protected

  def stream_name
    "patients_#{user.office.id}"
  end
end