# == Schema Information
#
# Table name: doctor_histories
#
#  id         :integer          not null, primary key
#  doctor_id  :integer
#  patient_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DoctorHistory < ApplicationRecord
  belongs_to :doctor, class_name: 'User'
  belongs_to :patient

  after_save { self.patient.update_attribute(:updated_at, Time.now)}
end
