# == Schema Information
#
# Table name: alerts
#
#  id              :integer          not null, primary key
#  resource_type   :string(255)
#  resource_id     :integer
#  recipient_type  :string(255)      not null
#  recipient_id    :integer          not null
#  message_title   :text(65535)
#  message_content :text(65535)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Alert < ApplicationRecord
  include Filterable
  belongs_to :resource, polymorphic: true, optional: true
  belongs_to :recipient, polymorphic: true
  has_many :alert_acks
  accepts_nested_attributes_for :resource

  after_create { BroadcastAlertJob.perform_later(self) }
end
