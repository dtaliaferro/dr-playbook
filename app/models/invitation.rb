# == Schema Information
#
# Table name: invitations
#
#  id          :integer          not null, primary key
#  office_id   :integer
#  role        :integer          default("disabled"), not null
#  signup_code :string(255)      not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Invitation < ApplicationRecord
  include HasRole
  
  belongs_to :office
end
