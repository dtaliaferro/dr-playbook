# == Schema Information
#
# Table name: patients
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  status             :integer          default("arrived"), not null
#  pt_parent          :string(255)
#  staff_id           :integer
#  exam_type          :integer
#  treatments         :integer          default(0), not null
#  pt_notes           :text(65535)
#  referral           :string(255)
#  arrived_at         :string(255)
#  appointment_at     :string(255)
#  departure_at       :string(255)
#  room               :string(255)
#  age                :integer
#  blood_pressure     :string(255)
#  doctor_id          :integer
#  procedure          :string(255)
#  objections         :integer          default(0), not null
#  health_history     :text(65535)
#  dr_history         :integer
#  pt_misc            :integer          default(0), not null
#  same_day_treatment :string(255)
#  complaint          :string(255)
#  concerns           :string(255)
#  concerns_other     :string(255)
#  next_visit         :string(255)
#  values             :integer          default(0), not null
#  ocs                :string(255)
#  tmj                :string(255)
#  perio              :string(255)
#  imaging            :string(255)
#  office_id          :integer          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Patient < ApplicationRecord
  include FlagShihTzu

  has_many :alerts, as: :resource
  belongs_to :office
  belongs_to :staff, class_name: 'User'
  belongs_to :doctor, class_name: 'User'
  has_many :doctor_histories
  has_many :past_doctors, through: :doctor_histories, source: :doctor
  validates_presence_of :office

  accepts_nested_attributes_for :doctor_histories

  enum status: [:arrived, :staff, :waiting, :doctor, :complete]

  enum exam_type: [:periodic, :limited, :comprehensive, :comprehensive_new]

  TREATMENTS_FLAGS =  { 1 => :fmx,
                        2 => :pano,
                        3 => :bwx,
                        4 => :pas,
                        5 => :pro,
                        6 => :perio_treatment }

  OBJECTIONS_FLAGS =  { 1 => :fear,
                        2 => :time,
                        3 => :budget,
                        4 => :urgency,
                        5 => :pt_trust }

  PT_MISC_FLAGS = { 1 => :new_pt,
                    2 => :pt_time,
                    3 => :priority,
                    4 => :notes_done,
                    5 => :same_day }

  VALUES_FLAGS =  { 1 => :cosmetic,
                    2 => :function,
                    3 => :comfort,
                    4 => :longevity }

  has_flags TREATMENTS_FLAGS.merge( { :column => 'treatments',
                                      :bang_methods => true })
  has_flags OBJECTIONS_FLAGS.merge({  :column => 'objections',
                                      :bang_methods => true })
  has_flags PT_MISC_FLAGS.merge({ :column => 'pt_misc',
                                  :bang_methods => true })
  has_flags VALUES_FLAGS.merge({ :column => 'values',
                                  :bang_methods => true })

  after_create { BroadcastPatientJob.perform_later(self, "created") }
  after_update { BroadcastPatientJob.perform_later(self, "updated") }
  before_destroy :delete_alerts_and_broadcast_job

  def delete_alerts_and_broadcast_job
    self.alerts.destroy_all
    @h = self.attributes.as_json
    BroadcastPatientJob.perform_later(@h, "destroyed")
  end
end
