# == Schema Information
#
# Table name: alert_acks
#
#  id             :integer          not null, primary key
#  recipient_type :string(255)      not null
#  recipient_id   :integer          not null
#  alert_id       :integer          not null
#

class AlertAck < ApplicationRecord
  belongs_to :recipient, polymorphic: true
  belongs_to :alert
end
