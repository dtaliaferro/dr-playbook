module HasRole
  extend ActiveSupport::Concern
  included do
    enum role: [:disabled, :staff, :doctor, :admin, :super_admin]
  end
end