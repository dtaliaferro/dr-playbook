# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  email           :string(255)
#  password_digest :string(255)
#  role            :integer          default("disabled"), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  office_id       :integer
#  token           :string(255)
#


class User < ApplicationRecord
  include HasRole
  include Filterable
  has_secure_password
  belongs_to :office
  has_many :alerts, as: :recipient
  has_many :alert_acks, as: :recipient
  has_many :dismissed_alerts, through: :alert_acks, source: :alert


  validates_presence_of :office
  
  # Returns all alerts that haven't yet been acknowledged
  def unacked_alerts
    acked_alert_ids = AlertAck.where(recipient_type: 'User', 
        recipient_id: self.id).pluck(:alert_id) || 0

    acked_alert_ids = [0] if acked_alert_ids.empty?

    unacked_alerts = Alert.where(recipient_type: 'User', 
      recipient_id: self.id).where('id NOT IN (?)', acked_alert_ids)

    unacked_alerts
      
  end

  def try_apn(alert)
    p "trying apn"
    if self.token?
      apn = Houston::Client.production
      apn.certificate = ENV.fetch("APN_CERT") || File.read("#{Rails.root}/config/ssl/devPush.pem")

      notification = Houston::Notification.new(device: self.token)
      notification.alert = alert.message_content
      notification.content_available = true
      notification.sound = 'default'

      apn.push(notification)
      p "APN Success!"
      puts "APN Error: #{notification.error}." if notification.error
    end
  end
end
