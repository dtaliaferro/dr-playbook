# == Schema Information
#
# Table name: offices
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  active     :boolean          default(TRUE), not null
#  num_users  :integer          default(0), not null
#

class Office < ApplicationRecord
  has_many :users
  has_many :invitations
  has_many :patients
  has_many :alerts, as: :recipient
end
