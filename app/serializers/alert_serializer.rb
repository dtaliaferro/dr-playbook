# == Schema Information
#
# Table name: alerts
#
#  id              :integer          not null, primary key
#  resource_type   :string(255)
#  resource_id     :integer
#  recipient_type  :string(255)      not null
#  recipient_id    :integer          not null
#  message_title   :text(65535)
#  message_content :text(65535)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class AlertSerializer < ActiveModel::Serializer
  attributes :id, :message_title, :message_content, :created_at, :resource_id

  has_one :resource, polymorphic: true
  has_one :recipient,  polymorphic: true

end
