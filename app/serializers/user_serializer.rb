# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  email           :string(255)
#  password_digest :string(255)
#  role            :integer          default("disabled"), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  office_id       :integer
#  token           :string(255)
#

class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :role, :office_id, :token, :created_at, :updated_at
end
