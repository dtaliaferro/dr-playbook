# == Schema Information
#
# Table name: patients
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  status             :integer          default("arrived"), not null
#  pt_parent          :string(255)
#  staff_id           :integer
#  exam_type          :integer
#  treatments         :integer          default(0), not null
#  pt_notes           :text(65535)
#  referral           :string(255)
#  arrived_at         :string(255)
#  appointment_at     :string(255)
#  departure_at       :string(255)
#  room               :string(255)
#  age                :integer
#  blood_pressure     :string(255)
#  doctor_id          :integer
#  procedure          :string(255)
#  objections         :integer          default(0), not null
#  health_history     :text(65535)
#  dr_history         :integer
#  pt_misc            :integer          default(0), not null
#  same_day_treatment :string(255)
#  complaint          :string(255)
#  concerns           :string(255)
#  concerns_other     :string(255)
#  next_visit         :string(255)
#  values             :integer          default(0), not null
#  ocs                :string(255)
#  tmj                :string(255)
#  perio              :string(255)
#  imaging            :string(255)
#  office_id          :integer          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class PatientSerializer < ActiveModel::Serializer
  has_many :alerts, as: :resource
  # belongs_to :office
  belongs_to :staff, class_name: 'User', serializer: UserSerializer
  belongs_to :doctor, class_name: 'User', serializer: UserSerializer
  has_many :doctor_histories
  has_many :past_doctors, through: :doctor_histories, source: :doctor

  attributes :id, :name, :status, :pt_parent, :staff_id, :doctor_id,
          :exam_type, :pt_notes, :referral, :arrived_at, :appointment_at, 
          :departure_at, :room, :age, :blood_pressure, :doctor_id, :procedure, 
          :health_history, :complaint, :concerns, :concerns_other, 
          :ocs, :tmj, :perio, :imaging, :office_id, :treatments, 
          :objections, :pt_misc, :values, :staff, :doctor, :same_day_treatment, :next_visit


  def treatments
    treatments_flags = []
    treatments_flags << "fmx" if object.fmx? 
    treatments_flags << "pano" if object.pano?
    treatments_flags << "bwx" if object.bwx? 
    treatments_flags << "pas" if object.pas? 
    treatments_flags << "pro" if object.pro? 
    treatments_flags << "perio_treatment" if object.perio_treatment? 

    treatments_flags

  end

  def objections
    objections_flags = []

    objections_flags << "fear" if object.fear?
    objections_flags << "time" if object.time?
    objections_flags << "budget" if object.budget?
    objections_flags << "urgency" if object.urgency?
    objections_flags << "pt_trust" if object.pt_trust?

    objections_flags
  end

  def pt_misc
    pt_misc_flags = []

    pt_misc_flags << "new_pt" if object.new_pt?
    pt_misc_flags << "pt_time" if object.pt_time?
    pt_misc_flags << "priority" if object.priority?
    pt_misc_flags << "notes_done" if object.notes_done?
    pt_misc_flags << "same_day" if object.same_day?

    pt_misc_flags
  end

  def values
    values_flags = []

    values_flags << "cosmetic" if object.cosmetic?
    values_flags << "function" if object.function?
    values_flags << "comfort" if object.comfort?
    values_flags << "longevity" if object.longevity?

    values_flags
  end
end
