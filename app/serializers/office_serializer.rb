# == Schema Information
#
# Table name: offices
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  active     :boolean          default(TRUE), not null
#  num_users  :integer          default(0), not null
#

class OfficeSerializer < ActiveModel::Serializer
  attributes :id, :name, :active, :num_users, :total_num_users
  has_many :users
  has_many :invitations
  has_many :alerts
  has_many :patients

  def total_num_users
    object.users.count
  end
end
