class RecipientSerializer < ActiveModel::Serializer
  def self.serializer_for(model, options)
    return UserSerializer if model.class == 'User'
    return OfficeSerializer if model.class == 'Office'

    super
  end
end