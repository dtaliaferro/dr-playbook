class ResourceSerializer < ActiveModel::Serializer
  def self.serializer_for(model, options)
    return PatientSerializer if model.class == 'Patient'
  end
end