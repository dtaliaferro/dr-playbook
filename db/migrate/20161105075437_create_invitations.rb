class CreateInvitations < ActiveRecord::Migration[5.0]
  def change
    create_table :invitations do |t|
      t.references :office, foreign_key: true, index: true
      t.integer :role, null: false, default: 0, index: true
      t.string :signup_code, null: false
      t.timestamps
    end

   
  end
end
