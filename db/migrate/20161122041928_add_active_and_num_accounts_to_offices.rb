class AddActiveAndNumAccountsToOffices < ActiveRecord::Migration[5.0]
  def change
    add_column :offices, :active, :boolean, null: false, default: true
    add_column :offices, :num_users, :integer, null: false, default: 0
  end
end
