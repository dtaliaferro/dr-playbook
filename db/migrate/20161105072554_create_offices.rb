class CreateOffices < ActiveRecord::Migration[5.0]
  def change
    create_table :offices do |t|
      t.string :name

      t.timestamps
    end

    add_reference :users, :office, foreign_key: true, index: true
  end
end
