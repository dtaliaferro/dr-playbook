class CreatePatients < ActiveRecord::Migration[5.0]
  def change
    create_table :patients do |t|
      t.string :name
      t.integer :status, null: false, default: 0
      t.string :pt_parent
      t.references :staff, references: :user
      t.integer :exam_type
      t.integer :treatments, null: false, default: 0
      t.text :pt_notes
      t.string :referral
      t.datetime :arrived_at
      t.datetime :appointment_at
      t.datetime :departure_at
      t.string :room
      t.integer :age
      t.string :blood_pressure
      t.references :doctor, references: :user
      t.string :procedure
      t.integer :objections, null: false, default: 0
      t.text :health_history
      t.integer :dr_history
      t.integer :pt_misc, null: false, default: 0
      t.string :complaint
      t.string :concerns
      t.string :concerns_other
      t.integer :values, null: false, default: 0
      t.string :ocs
      t.string :tmj
      t.string :perio
      t.string :imaging
      
      t.references :office, null: false, index: true
      t.timestamps
    end
  end
end
