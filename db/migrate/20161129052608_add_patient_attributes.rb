class AddPatientAttributes < ActiveRecord::Migration[5.0]
  def change
    add_column :patients, :same_day_treatment, :string, after: :pt_misc
    add_column :patients, :next_visit, :string, after: :concerns_other
  end
end
