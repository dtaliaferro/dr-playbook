class CreateDoctorHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :doctor_histories do |t|
      t.references :doctor, references: :user
      t.references :patient
      t.timestamps
    end
  end
end
