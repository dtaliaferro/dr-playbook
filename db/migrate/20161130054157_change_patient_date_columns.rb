class ChangePatientDateColumns < ActiveRecord::Migration[5.0]
  def up
    change_column :patients, :arrived_at, :string
    change_column :patients, :departure_at, :string
    change_column :patients, :appointment_at, :string
  end

  def down
    change_column :patients, :arrived_at, :datetime
    change_column :patients, :departure_at, :datetime
    change_column :patients, :appointment_at, :datetime
  end
end
