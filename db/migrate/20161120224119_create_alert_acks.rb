class CreateAlertAcks < ActiveRecord::Migration[5.0]
  def change
    create_table :alert_acks do |t|
      t.references :recipient, polymorphic: true, index: true, null: false
      t.integer :alert_id, index: true, null: false
    end
  end
end
