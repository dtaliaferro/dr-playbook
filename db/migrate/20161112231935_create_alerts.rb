class CreateAlerts < ActiveRecord::Migration[5.0]
  def change
    create_table :alerts do |t|
      t.references :resource, polymorphic: true, index: true
      t.references :recipient, polymorphic: true, index: true, null: false
      t.text :message_title
      t.text :message_content
      t.timestamps
    end
  end
end
