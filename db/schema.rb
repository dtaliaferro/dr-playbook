# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161208014729) do

  create_table "alert_acks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "recipient_type", null: false
    t.integer "recipient_id",   null: false
    t.integer "alert_id",       null: false
    t.index ["alert_id"], name: "index_alert_acks_on_alert_id", using: :btree
    t.index ["recipient_type", "recipient_id"], name: "index_alert_acks_on_recipient_type_and_recipient_id", using: :btree
  end

  create_table "alerts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "resource_type"
    t.integer  "resource_id"
    t.string   "recipient_type",                null: false
    t.integer  "recipient_id",                  null: false
    t.text     "message_title",   limit: 65535
    t.text     "message_content", limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["recipient_type", "recipient_id"], name: "index_alerts_on_recipient_type_and_recipient_id", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_alerts_on_resource_type_and_resource_id", using: :btree
  end

  create_table "doctor_histories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "doctor_id"
    t.integer  "patient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doctor_id"], name: "index_doctor_histories_on_doctor_id", using: :btree
    t.index ["patient_id"], name: "index_doctor_histories_on_patient_id", using: :btree
  end

  create_table "invitations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "office_id"
    t.integer  "role",        default: 0, null: false
    t.string   "signup_code",             null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["office_id"], name: "index_invitations_on_office_id", using: :btree
    t.index ["role"], name: "index_invitations_on_role", using: :btree
  end

  create_table "offices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "active",     default: true, null: false
    t.integer  "num_users",  default: 0,    null: false
  end

  create_table "patients", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "status",                           default: 0, null: false
    t.string   "pt_parent"
    t.integer  "staff_id"
    t.integer  "exam_type"
    t.integer  "treatments",                       default: 0, null: false
    t.text     "pt_notes",           limit: 65535
    t.string   "referral"
    t.string   "arrived_at"
    t.string   "appointment_at"
    t.string   "departure_at"
    t.string   "room"
    t.integer  "age"
    t.string   "blood_pressure"
    t.integer  "doctor_id"
    t.string   "procedure"
    t.integer  "objections",                       default: 0, null: false
    t.text     "health_history",     limit: 65535
    t.integer  "dr_history"
    t.integer  "pt_misc",                          default: 0, null: false
    t.string   "same_day_treatment"
    t.string   "complaint"
    t.string   "concerns"
    t.string   "concerns_other"
    t.string   "next_visit"
    t.integer  "values",                           default: 0, null: false
    t.string   "ocs"
    t.string   "tmj"
    t.string   "perio"
    t.string   "imaging"
    t.integer  "office_id",                                    null: false
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.index ["doctor_id"], name: "index_patients_on_doctor_id", using: :btree
    t.index ["office_id"], name: "index_patients_on_office_id", using: :btree
    t.index ["staff_id"], name: "index_patients_on_staff_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.integer  "role",            default: 0, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "office_id"
    t.string   "token"
    t.index ["office_id"], name: "index_users_on_office_id", using: :btree
  end

  add_foreign_key "invitations", "offices"
  add_foreign_key "users", "offices"
end
