# == Schema Information
#
# Table name: invitations
#
#  id          :integer          not null, primary key
#  office_id   :integer
#  role        :integer          default("disabled"), not null
#  signup_code :string(255)      not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :invitation do
    office
    role :staff
    signup_code SecureRandom.hex(10)
  end
end
