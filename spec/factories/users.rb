# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  email           :string(255)
#  password_digest :string(255)
#  role            :integer          default("disabled"), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  office_id       :integer
#  token           :string(255)
#

require 'bcrypt'

FactoryGirl.define do
  factory :user do
    name Faker::Name.name
    email Faker::Internet.email
    password_digest BCrypt::Password.create('password')
    role :staff
    office
  end
end
