# == Schema Information
#
# Table name: offices
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  active     :boolean          default(TRUE), not null
#  num_users  :integer          default(0), not null
#

FactoryGirl.define do
  factory :office do
    name Faker::Company.name


    factory :office_with_users do
      transient do
        users_count 5
      end

      after(:create) do |office, evaluator|
        create_list(:user, evaluator.users_count, office: office)
      end
    end
  end
end
