# == Schema Information
#
# Table name: alerts
#
#  id              :integer          not null, primary key
#  resource_type   :string(255)
#  resource_id     :integer
#  recipient_type  :string(255)      not null
#  recipient_id    :integer          not null
#  message_title   :text(65535)
#  message_content :text(65535)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :office_alert, class: "Alert" do
    association :recipient, factory: :office
    message_title Faker::Name.title
    message_content Faker::Hipster.sentence
  end

  factory :user_alert, class: "Alert" do
    association :recipient, factory: :user
    message_title Faker::Name.title
    message_content Faker::Hipster.sentence
  end  
end
