require 'rails_helper'

describe "Invitation delete" do

  before(:each) do
    @o = create(:office)
    @u = create(:user, role: :super_admin, office: @o)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @i = create(:invitation)
  end

  it 'deletes an invitation when authenticated' do

    delete "/v1/invitations/#{@i.id}", 
      params: { }, 
      headers: @auth_token

    expect(response).to be_success
    expect(Invitation.where(id: @i.id).count).to eq(0)
  end

  it 'does not delete an invitation when unauthenticated' do

    delete "/v1/invitations/#{@i.id}", 
      params: { }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
    expect(Invitation.where(id: @i.id).count).to eq(1)
  end
end