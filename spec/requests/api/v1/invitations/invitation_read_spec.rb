require 'rails_helper'

describe "Invitation read" do

  before(:each) do
    @o = create(:office)
    @u = create(:user, role: :super_admin, office: @o)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @i = create(:invitation)
  end

  it 'shows an invitation when authenticated' do
    get "/v1/invitations/#{@i.id}", 
      params: { }, 
      headers: @auth_token

    expect(response).to be_success
    expect(json[:office_id]).to eq(@i.office_id)
    expect(json[:id]).to eq(@i.id)
    expect(json[:signup_code]).to eq(@i.signup_code)
  end

  it 'does not show an invitation when unauthenticated' do
    get "/v1/invitations/#{@i.id}", 
      params: { }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end

  it 'shows an index of all invitations when authenticated' do
    get "/v1/invitations/",
    headers: @auth_token

    expect(response).to be_success
    expect(json.count).to eq(Invitation.count)
  end

  it 'does not show an index of all invitations when not authenticated' do
    get "/v1/invitations/",
    headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end

end