require 'rails_helper'

describe "Invitation update" do

  before(:each) do
    @u = create(:user, role: :super_admin)
    @signup_code = 'CODE123'
    @sc = create(:invitation, signup_code: @signup_code)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @i = create(:invitation)

    @new_code = SecureRandom.hex(10)
  end

  it 'updates an invitation when authenticated' do
    

    patch "/v1/invitations/#{@i.id}", 
      params: { invitation: { signup_code: @new_code } }, 
      headers: @auth_token

    expect(response).to be_success
    expect(json[:signup_code]).to eq(@new_code)
    expect(json[:id]).to eq(@i.id)
  end

  it 'does not update an invitation when unauthenticated' do
    @new_name = Faker::Name.name

    patch "/v1/users/#{@u.id}", 
      params: { invitation: { signup_code: @new_code } }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end
end