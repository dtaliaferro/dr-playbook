require 'rails_helper'

describe "Invitation creation" do

  before(:each) do
    @o = create(:office)
    @u = create(:user, role: :super_admin, office: @o)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }
  end

  it 'creates an invitation when correctly authenticated' do
    post '/v1/invitations/', 
      params: { 
        invitation: { 
          office_id: @o.id, 
          role: :staff,
          signup_code: SecureRandom.hex(5)
        }
      }, headers: @auth_token

    expect(response).to be_success
    expect(Invitation.last.office_id).to eq(@o.id)
    expect(Invitation.last.role).to eq("staff")

  end

  it 'does not create an invitation when unauthenticated' do
    post '/v1/invitations/',
      params: { 
        invitation: { 
          office_id: @o.id, 
          role: :staff,
          signup_code: SecureRandom.hex(5)
        }
      }

    expect(response).to be_unauthorized
  end
end