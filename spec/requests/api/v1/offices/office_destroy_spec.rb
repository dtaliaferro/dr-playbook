require 'rails_helper'

describe "Office delete" do

  before(:each) do
    @o = create(:office)
    @u = create(:user, role: :super_admin, office: @o)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @o2 = create(:office)
  end

  it 'deletes an office when authenticated' do

    delete "/v1/offices/#{@o2.id}", 
      params: { }, 
      headers: @auth_token

    expect(response).to be_success
    expect(Office.where(id: @o2.id).count).to eq(0)
  end

  it 'does not delete an office when unauthenticated' do

    delete "/v1/offices/#{@o2.id}", 
      params: { }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
    expect(Office.where(id: @o2.id).count).to eq(1)
  end
end