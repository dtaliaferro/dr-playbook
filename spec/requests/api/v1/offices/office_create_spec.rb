require 'rails_helper'

describe "Office creation" do

  before(:each) do
    @o = create(:office)
    @u = create(:user, role: :super_admin, office: @o)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @office_name = Faker::Company.name
  end

  it 'creates an office when correctly authenticated' do

    post '/v1/offices/', 
      params: { 
        office: { 
          name: @office_name
        }
      }, headers: @auth_token

    expect(response).to be_success
    expect(Office.last.name).to eq(@office_name)

  end

  it 'does not create an office when unauthenticated' do
    post '/v1/offices/', 
      params: { 
        office: { 
          name: @office_name
        }
      }

    expect(response).to be_unauthorized
  end
end