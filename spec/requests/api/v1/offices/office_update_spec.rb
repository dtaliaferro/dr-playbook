require 'rails_helper'

describe "Office update" do

  before(:each) do
    @u = create(:user, role: :super_admin)
    @signup_code = 'CODE123'
    @sc = create(:invitation, signup_code: @signup_code)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @o = create(:office)
    @new_name = Faker::Company.name
  end

  it 'updates an office when authenticated' do
    
    patch "/v1/offices/#{@o.id}", 
      params: { office: { name: @new_name } }, 
      headers: @auth_token

    expect(response).to be_success
    expect(json[:name]).to eq(@new_name)
    expect(json[:id]).to eq(@o.id)
  end

  it 'does not update an office when unauthenticated' do

    patch "/v1/offices/#{@o.id}", 
      params: { office: { name: @new_name } }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
    expect(Office.find(@o.id).name).to_not eq(@new_name)
  end
end