require 'rails_helper'

describe "Office read" do

  before(:each) do
    @o = create(:office)
    @u = create(:user, role: :super_admin, office: @o)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @o2 = create(:office)
  end

  it 'shows an office when authenticated' do
    get "/v1/offices/#{@o2.id}", 
      params: { }, 
      headers: @auth_token

    expect(response).to be_success
    expect(json[:id]).to eq(@o2.id)
    expect(json[:name]).to eq(@o2.name)
  end

  it 'does not show an office when unauthenticated' do
    get "/v1/offices/#{@o2.id}", 
      params: { }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end

  it 'shows an index of all offices when authenticated' do
    get "/v1/offices/",
    headers: @auth_token

    expect(response).to be_success
    expect(json.count).to eq(Office.count)
  end

  it 'does not show an index of all offices when not authenticated' do
    get "/v1/offices/",
    headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end

end