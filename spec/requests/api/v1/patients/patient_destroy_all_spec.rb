require 'rails_helper'

describe "Patient delete" do

  before(:each) do
    @u = create(:user, role: :super_admin)
    @signup_code = 'CODE123'
    @sc = create(:invitation, signup_code: @signup_code)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @o = create(:office)
    (1..10).each do |i|
      create(:patient, office_id: @o.id)
    end

    @patient_count = @o.patients.count
  end

  it 'deletes a patient when authenticated' do
    delete "/v1/offices/#{@o.id}/patients/delete_all", 
      params: { }, 
      headers: @auth_token

    expect(response).to be_success
    expect(Patient.where(office_id: @o.id).count).to eq(0)
  end

  it 'does not delete a patient when unauthenticated' do
    delete "/v1/offices/#{@o.id}/patients/delete_all", 
      params: { }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
    expect(Patient.where(office_id: @o.id).count).to eq(@patient_count)
  end
end