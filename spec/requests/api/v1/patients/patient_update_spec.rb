require 'rails_helper'

describe "User update" do

  before(:each) do
    @u = create(:user, role: :super_admin)
    @signup_code = 'CODE123'
    @sc = create(:invitation, signup_code: @signup_code)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @p = create(:patient)

  end

  it 'updates a patient when authenticated' do
    @new_name = Faker::Name.name

    patch "/v1/patients/#{@p.id}", 
      params: { patient: { name: @new_name,
                           treatments: ["pano", "pro"]   } }, 
      headers: @auth_token

    expect(response).to be_success
    expect(Patient.find(@p.id).pano?).to eq(true)
    expect(Patient.find(@p.id).bwx?).to eq(false)
    expect(json[:id]).to eq(@p.id)
  end

  it 'does not update a user when unauthenticated' do
    @new_name = Faker::Name.name

    patch "/v1/patients/#{@p.id}", 
      params: { patient: { name: @new_name } }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end
end