require 'rails_helper'

describe "Patient read" do

  before(:each) do
    @u = create(:user, role: :super_admin)
    @signup_code = 'CODE123'
    @sc = create(:invitation, signup_code: @signup_code)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }
    
    @p = create(:patient)
  end

  it 'shows a patient when authenticated' do
    get "/v1/patients/#{@p.id}", 
      params: { }, 
      headers: @auth_token

    expect(response).to be_success
    expect(json[:name]).to eq(@p.name)
    expect(json[:id]).to eq(@p.id)
  end

  it 'does not show a patient when unauthenticated' do
    get "/v1/patients/#{@p.id}", 
      params: { }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end

  it 'shows an index of all patients when authenticated' do
    get "/v1/patients/",
    headers: @auth_token

    expect(response).to be_success
    expect(json.count).to eq(Patient.count)
  end

  it 'does not index all patients when not authenticated' do
    get "/v1/patients/",
    headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end

end