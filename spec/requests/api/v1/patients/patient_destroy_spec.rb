require 'rails_helper'

describe "Patient delete" do

  before(:each) do
    @u = create(:user, role: :super_admin)
    @signup_code = 'CODE123'
    @sc = create(:invitation, signup_code: @signup_code)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @p = create(:patient)
    @a = create(:user_alert, resource: @p)
  end

  it 'deletes a patient when authenticated' do
    delete "/v1/patients/#{@p.id}", 
      params: { }, 
      headers: @auth_token

    expect(response).to be_success
    expect(Patient.where(id: @p.id).count).to eq(0)
    expect(Alert.where(resource_id: @p.id, resource_type: 'Patient').count).to eq(0)
  end

  it 'does not delete a patient when unauthenticated' do
    delete "/v1/patients/#{@p.id}", 
      params: { }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
    expect(Patient.where(id: @p.id).count).to eq(1)
    expect(Alert.where(resource_id: @p.id, resource_type: 'Patient').count).to eq(1)

  end
end