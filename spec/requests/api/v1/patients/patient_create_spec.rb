require 'rails_helper'

describe "Patient creation" do

  before(:each) do
    @u = create(:user, role: :super_admin)
    @signup_code = 'CODE123'
    @sc = create(:invitation, signup_code: @signup_code)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }
    @old_user_count = User.count
    @name = Faker::Name.name
    @email = Faker::Internet.email
    @password = 'testtest'

    @o = create(:office)
    @s = create(:user, role: :staff, office: @o)
    @d = create(:user, role: :doctor, office: @o)

  end

  it 'creates a patient when correctly authenticated' do
    post '/v1/patients/', 
      params: { 
        patient: {
        name:@name,
        status: "arrived",
        pt_parent:"Patient Parent",
        staff_id:@s.id,
        exam_type:"periodic",
        treatments:["fmx", "pano"],
        pt_notes:"Patient is alive",
        referral:"Dr. Referrer",
        arrived_at: "2016-11-11 11:11:11",
        appointment_at: "2016-11-11 11:11:12",
        departure_at: "2016-11-11 11:11:13",
        doctor_history_ids: [@d.id],
        room:3,
        age:3,
        blood_pressure:"120/44",
        doctor_id:@d.id,
        procedure:"cavities",
        objections: ["fear", "time"],
        health_history:"healthy",
        pt_misc:["new_pt", "priority"],
        complaint:"Too loud",
        concerns:"Too quiet",
        concerns_other:"Way too quiet",
        values:["cosmetic", "comfort"],
        ocs: "What",
        tmj: "Do",
        perio: "These",
        imaging: "Mean",
        office_id: @o.id
        }
      }, headers: @auth_token

    expect(response).to be_success
    expect(Patient.last.name).to eq(@name)
    expect(Patient.last.pano?).to eq(true)
    expect(Patient.last.bwx?).to eq(false)
    expect(Patient.last.fear?).to eq(true)
    expect(Patient.last.urgency?).to eq(false)
    expect(Patient.last.new_pt?).to eq(true)
    expect(Patient.last.same_day?).to eq(false)
    expect(Patient.last.cosmetic?).to eq(true)
    expect(Patient.last.longevity?).to eq(false)
    expect(Patient.last.doctor_history_ids).to include(@d.id)

  end

  it 'does not create a user when unauthenticated' do
    post '/v1/patients/', 
      params: { 
        patient: {
        name:@name,
        status: "arrived",
        pt_parent:"Patient Parent",
        staff_id:2,
        exam_type:"periodic",
        treatments:["fmx", "pano"],
        pt_notes:"Patient is alive",
        referral:"Dr. Referrer",
        arrived_at: "2016-11-11 11:11:11",
        appointment_at: "2016-11-11 11:11:12",
        departure_at: "2016-11-11 11:11:13",
        room:3,
        age:3,
        blood_pressure:"120/44",
        doctor_id:1,
        procedure:"cavities",
        objections: ["fear", "time"],
        health_history:"healthy",
        pt_misc:["new_pt", "priority"],
        complaint:"Too loud",
        concerns:"Too quiet",
        concerns_other:"Way too quiet",
        values:["cosmetic", "comfort"],
        ocs: "What",
        tmj: "Do",
        perio: "These",
        imaging: "Mean",
        office_id: 2
        }
      }

    expect(response).to be_unauthorized
  end

end