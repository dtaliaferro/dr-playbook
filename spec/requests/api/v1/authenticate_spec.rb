require 'rails_helper'

describe "User authentication" do
  it 'returns a token when correctly authenticated' do
    @u = create(:user)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json

    expect(response).to be_success

    expect(json).to have_key(:auth_token)
    expect(json[:user][:id]).to eq(@u.id)
    expect(json[:user][:password_digest]).to be_nil
  end

  it 'does not return a token when correctly authenticated' do
    @u = create(:user)

    params = { email: @u.email, password: 'wrongpassword' }

    post '/v1/authenticate', params: params, as: :json

    expect(response).to be_unauthorized

    expect(json[:auth_token]).to be_nil
    
    expect(json).to eq(error: { user_authentication: [ 'invalid credentials' ] })
  end
end