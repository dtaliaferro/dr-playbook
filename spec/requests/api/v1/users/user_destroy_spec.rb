require 'rails_helper'

describe "User delete" do

  before(:each) do
    @u = create(:user, role: :super_admin)
    @signup_code = 'CODE123'
    @sc = create(:invitation, signup_code: @signup_code)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }
    @old_user_count = User.count
    @name = Faker::Name.name
    @email = Faker::Internet.email
    @password = 'testtest'

  end

  it 'deletes a user when authenticated' do
    @new_name = Faker::Name.name

    delete "/v1/users/#{@u.id}", 
      params: { }, 
      headers: @auth_token

    expect(response).to be_success
    expect(User.where(id: @u.id).count).to eq(0)
  end

  it 'does not delete a user when unauthenticated' do
    @new_name = Faker::Name.name

    delete "/v1/users/#{@u.id}", 
      params: { }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end
end