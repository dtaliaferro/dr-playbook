require 'rails_helper'

describe "User creation" do

  before(:each) do
    @u = create(:user, role: :super_admin)
    @signup_code = 'CODE123'
    @sc = create(:invitation, signup_code: @signup_code)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }
    @old_user_count = User.count
    @name = Faker::Name.name
    @email = Faker::Internet.email
    @password = 'testtest'

    @o = create(:office)
  end

  it 'creates a user when correctly authenticated' do
    post '/v1/users/', 
      params: { 
        user: { 
          name: @name, 
          email: @email, 
          password: @password,
          office_id: @o.id,
          role: "staff"
        }
      }, headers: @auth_token

    expect(response).to be_success
    expect(User.last.name).to eq(@name)
    expect(User.last.role).to eq("staff")

  end

  it 'does not create a user when unauthenticated' do
    post '/v1/users/', 
      params: { 
        user: { 
          name: @name, 
          email: @email, 
          password: @password,
          office_id: @o.id, 
        }
      }

    expect(response).to be_unauthorized
  end

  it 'does not create a user when the signup code is incorrect' do
    post '/v1/users/', 
      params: { 
        user: { 
          name: @name, 
          email: @email, 
          password: @password, 
        },
        signup_code: 'WRONG'
      }, headers: @auth_token

    expect(response).to be_unprocessable
  end
end