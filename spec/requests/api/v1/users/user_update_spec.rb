require 'rails_helper'

describe "User update" do

  before(:each) do
    @u = create(:user, role: :super_admin)
    @signup_code = 'CODE123'
    @sc = create(:invitation, signup_code: @signup_code)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }
    @old_user_count = User.count
    @name = Faker::Name.name
    @email = Faker::Internet.email
    @password = 'testtest'

  end

  it 'updates a user when authenticated' do
    @new_name = Faker::Name.name

    patch "/v1/users/#{@u.id}", 
      params: { user: { name: @new_name } }, 
      headers: @auth_token

    expect(response).to be_success
    expect(json[:name]).to eq(@new_name)
    expect(json[:id]).to eq(@u.id)
  end

  it 'does not update a user when unauthenticated' do
    @new_name = Faker::Name.name

    patch "/v1/users/#{@u.id}", 
      params: { name: @new_name }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end
end