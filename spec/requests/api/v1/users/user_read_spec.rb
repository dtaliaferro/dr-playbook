require 'rails_helper'

describe "User read" do

  before(:each) do
    @u = create(:user, role: :super_admin)
    @signup_code = 'CODE123'
    @sc = create(:invitation, signup_code: @signup_code)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }
    @old_user_count = User.count
    @name = Faker::Name.name
    @email = Faker::Internet.email
    @password = 'testtest'

    @u2 = create(:user, role: :doctor)
  end

  it 'shows a user when authenticated' do
    get "/v1/users/#{@u.id}", 
      params: { }, 
      headers: @auth_token

    expect(response).to be_success
    expect(json[:name]).to eq(@u.name)
    expect(json[:id]).to eq(@u.id)
    expect(json[:password_digest]).to be_nil
  end

  it 'does not show a user when unauthenticated' do
    get "/v1/users/#{@u.id}", 
      params: { }, 
      headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end

  it 'shows an index of all users when authenticated' do
    get "/v1/users/",
    headers: @auth_token

    expect(response).to be_success
    expect(json.count).to eq(User.count)
  end

# TODO: why isn't this working?

=begin
  it 'filters users by role' do
    get "/v1/users/?role=doctor",
    headers: @auth_token

    expect(response).to be_success
    expect(json.count).to eq(User.doctor.count)
  end
=end
  it 'filters users by office and role' do
    get "/v1/offices/#{@u.office.id}/users/?role=doctor",
    headers: @auth_token

    expect(response).to be_success
    expect(json.count).to eq(User.where(office: @u.office, role: :doctor).count)
  end


  it 'does an index of all users when not authenticated' do
    get "/v1/users/",
    headers: { 'HTTP_AUTHORIZATION': 'wrong' }

    expect(response).to be_unauthorized
  end

end