require 'rails_helper'

describe "Patient alert creation" do

  before(:each) do
    @o = create(:office)
    @u = create(:user, role: :super_admin, office: @o)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @a = create(:user_alert, recipient: @u)
  end

  it 'deletes an alert when correctly authenticated' do

    delete "/v1/alerts/#{@a.id}/", 
    params: { }, 
    headers: @auth_token

    expect(response).to be_success
    expect(Alert.where(id: @a.id).count).to eq(0)

  end

  it 'does not delete an alert when unauthenticated' do
    @msg_title = Faker::Name.title
    @msg_content = Faker::Hipster.sentence

    delete "/v1/alerts/#{@a.id}/"

    expect(response).to be_unauthorized
    expect(Alert.where(id: @a.id).count).to eq(1)

  end
end