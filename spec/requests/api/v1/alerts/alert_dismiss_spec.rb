require 'rails_helper'

describe "Patient alert dismissal" do

  before(:each) do
    @o = create(:office)
    @u = create(:user, role: :super_admin, office: @o)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @p = create(:patient, office: @o)

    @a = create(:user_alert, recipient: @u)

  end

  it 'dismisses an alert when correctly authenticated' do
    expect(User.find(@u.id).unacked_alerts.pluck(:id)).to include(@a.id)

    post "/v1/alerts/#{@a.id}/dismiss", params: { }, headers: @auth_token

    expect(User.find(@u.id).unacked_alerts.pluck(:id)).to_not include(@a.id)
  end

end