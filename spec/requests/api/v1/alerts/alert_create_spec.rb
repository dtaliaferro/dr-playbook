require 'rails_helper'

describe "Patient alert creation" do

  before(:each) do
    @o = create(:office)
    @u = create(:user, role: :super_admin, office: @o)

    params = { email: @u.email, password: 'password' }

    post '/v1/authenticate', params: params, as: :json
    
    @auth_token = { 'HTTP_AUTHORIZATION': json[:auth_token] }

    @p = create(:patient, office: @o)
  end

  it 'creates an alert when correctly authenticated' do
    @msg_title = Faker::Name.title
    @msg_content = Faker::Hipster.sentence

    post "/v1/offices/#{@o.id}/alerts", 
      params: { 
        alert: { 
          message_title: @msg_title, 
          message_content:  @msg_content
        }
      }, headers: @auth_token

    expect(response).to be_success
    expect(@o.alerts.last.message_content).to eq(@msg_content)

  end

  it 'does not create an alert when unauthenticated' do
    @msg_title = Faker::Name.title
    @msg_content = Faker::Hipster.sentence

    post "/v1/offices/#{@o.id}/alerts", 
      params: { 
        alert: { 
          message_title: @msg_title, 
          message_content:  @msg_content
        }
      }

    expect(response).to be_unauthorized
  end
end