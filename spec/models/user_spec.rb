# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  email           :string(255)
#  password_digest :string(255)
#  role            :integer          default("disabled"), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  office_id       :integer
#  token           :string(255)
#

require 'rails_helper'

RSpec.describe User, type: :model do
  it "should have an email address, name, and password" do
    @u = create(:user)

    expect(User.where(name: @u.name)).to exist
  end

  it "should belong to an office" do
    @u = create(:user)

    expect(@u.office).to eq(Office.last)
  end

  it "must belong to an office" do
    expect(FactoryGirl.build(:user, role: :staff, office_id: nil)).to_not be_valid
  end
end
