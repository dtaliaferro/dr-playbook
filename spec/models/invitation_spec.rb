# == Schema Information
#
# Table name: invitations
#
#  id          :integer          not null, primary key
#  office_id   :integer
#  role        :integer          default("disabled"), not null
#  signup_code :string(255)      not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Invitation, type: :model do
  it "should have an office" do
    a = Invitation.reflect_on_association(:office)

    expect(a.macro).to eq(:belongs_to)
  end
end
