# == Schema Information
#
# Table name: patients
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  status             :integer          default("arrived"), not null
#  pt_parent          :string(255)
#  staff_id           :integer
#  exam_type          :integer
#  treatments         :integer          default(0), not null
#  pt_notes           :text(65535)
#  referral           :string(255)
#  arrived_at         :string(255)
#  appointment_at     :string(255)
#  departure_at       :string(255)
#  room               :string(255)
#  age                :integer
#  blood_pressure     :string(255)
#  doctor_id          :integer
#  procedure          :string(255)
#  objections         :integer          default(0), not null
#  health_history     :text(65535)
#  dr_history         :integer
#  pt_misc            :integer          default(0), not null
#  same_day_treatment :string(255)
#  complaint          :string(255)
#  concerns           :string(255)
#  concerns_other     :string(255)
#  next_visit         :string(255)
#  values             :integer          default(0), not null
#  ocs                :string(255)
#  tmj                :string(255)
#  perio              :string(255)
#  imaging            :string(255)
#  office_id          :integer          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'rails_helper'

RSpec.describe Patient, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
