# == Schema Information
#
# Table name: offices
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  active     :boolean          default(TRUE), not null
#  num_users  :integer          default(0), not null
#

require 'rails_helper'

RSpec.describe Office, type: :model do
  it "should have users" do
    create(:office_with_users, users_count: 10)

    expect(Office.last.users.count).to eq(10)
  end
end
