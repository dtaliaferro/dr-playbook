Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  scope module: 'api' do
    namespace :v1 do
      mount ActionCable.server => '/cable'
      post 'authenticate', to: 'authentication#authenticate'
      resources :users, model_name: 'User' do
        resources :alerts, only: [:create, :index]
      end
      resources :invitations
      resources :offices, model_name: 'Office' do
        resources :users, only: [:index]
        resources :alerts, only: [:create, :index]
        delete 'patients/delete_all', to: 'patients#delete_all'
      end
      resources :patients
      resources :alerts, only: [:show, :destroy]
      post 'alerts/:alert_id/dismiss', to: 'alerts#dismiss'
    end
  end
end
